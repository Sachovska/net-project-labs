﻿using System;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;

namespace DataAccessLayer.SerealizationProviders
{
    public class GenericProvider<T>
    {
        private MemoryStream _memoryStream;

        public GenericProvider()
        {
            _memoryStream = new MemoryStream();
        }

        public virtual bool AddFromConsole()
        {
            throw new NotImplementedException();
        }
        
        protected internal T Load(string fileName, T obj)
        {
            using (var fstream = File.OpenRead(fileName))
            {
                var array = new byte[fstream.Length];
                fstream.Read(array, 0, array.Length);
                var json = Encoding.Default.GetString(array);

                _memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(json));
                var ser = new DataContractJsonSerializer(obj.GetType());
                var deserializedObj = (T) ser.ReadObject(_memoryStream);
                
                _memoryStream.Close();
                fstream.Close();
                return deserializedObj;
            }
        }

        protected internal bool Save(string fileName, T obj)
        {
            var ser = new DataContractJsonSerializer(typeof(T));
            ser.WriteObject(_memoryStream, obj);
            var json = _memoryStream.ToArray();
            var objectToJson = Encoding.UTF8.GetString(json, 0, json.Length);
            var fstream = new FileStream(fileName, FileMode.OpenOrCreate);
            fstream.SetLength(0);
            var array = Encoding.Default.GetBytes(objectToJson);
            fstream.Write(array, 0, array.Length);
            fstream.Close();
            return array.Length != 0;
        }
    }
}
