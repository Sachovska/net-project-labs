﻿using System;
using System.ComponentModel.DataAnnotations;
using DataAccessLayer.Interfaces;
using Entities.Attributes;
using Entities.ModelsMemoryStream;

namespace DataAccessLayer.SerealizationProviders
{
    public class PaperProvider:GenericProvider<Paper>, IMemoryStreamSeralization<Paper>
    {
        private readonly AttributeValidator _validator;
        private readonly GenericProvider<Paper> _genericProvider;

        public PaperProvider()
        {
            _validator = new AttributeValidator();
            _genericProvider = new GenericProvider<Paper>();
        }

        public override bool AddFromConsole()
        {
            try
            {
                var paper = new Paper();
                Console.WriteLine("Set name:");
                paper.Name = Convert.ToString(Console.ReadLine());
                Console.WriteLine("Set author name:");
                var authorName = Convert.ToString(Console.ReadLine());
                Console.WriteLine("Set author surname:");
                var authorSurname = Convert.ToString(Console.ReadLine());
                Console.WriteLine("Set author date of bithday:");
                var authorDb = Convert.ToDateTime(Convert.ToString(Console.ReadLine()));
                paper.Author = new Person(1, authorName, authorSurname, authorDb);
                Console.WriteLine("Set date of publish: ");
                paper.DateOfPublish = Convert.ToDateTime(Convert.ToString(Console.ReadLine()));
                _validator.Validate(paper);
                Save("paper.txt", paper);
                return true;
            }
            catch (ValidationException ex)
            {
                Console.WriteLine(ex);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            return false;
        }

        public void CreateObj(string fileName, Paper obj)
        {
            _validator.Validate(obj);
            _genericProvider.Save(fileName, obj);
        }

        public Paper GetObj(string fileName)
        {
            var paper = new Paper();
            return _genericProvider.Load(fileName, paper);
        }
    }
}
