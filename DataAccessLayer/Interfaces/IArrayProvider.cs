﻿namespace DataAccessLayer.Interfaces
{
    public interface IArrayProvider
    {
        void CreateArray(int row, int column);
        int ShowTimeResult();
    }
}
