﻿namespace DataAccessLayer.Interfaces
{
    interface IMemoryStreamSeralization<T>
    {
        void CreateObj(string fileName, T obj);
        T GetObj(string fileName);
    }
}
