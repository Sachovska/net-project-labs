﻿using System;
using DataAccessLayer.Interfaces;
using Entities.Models;

namespace DataAccessLayer.Providers
{
    public class TwoSizeArrayProvider:IArrayProvider
    {
        private ResearchTeam[,] _researchTeam;
        private int _timeForExecute;

        public TwoSizeArrayProvider()
        {
            _researchTeam = new ResearchTeam[,] { };
            _timeForExecute = 0;
        }

        public void CreateArray(int row, int column)
        {
            _researchTeam = new ResearchTeam[row, column];
            var startTrackTime = Environment.TickCount;
            for (var i = 0; i < row; i++)
            {
                for (var j = 0; j < column; j++)
                {
                    _researchTeam[i, j] = new ResearchTeam();
                }
            }
            var endTrackTime = Environment.TickCount;
            _timeForExecute = endTrackTime - startTrackTime;
        }

        public int ShowTimeResult()
        {
            return _timeForExecute;
        }
    }
}
