﻿using System;
using DataAccessLayer.Interfaces;
using Entities.Models;

namespace DataAccessLayer.Providers
{
    public class RectangularSizeArray:IArrayProvider
    {
        private ResearchTeam[][] _researchTeam;
        private int _rows;
        private int _timeForExecute;

        public RectangularSizeArray()
        {
            _researchTeam = new ResearchTeam[][] { };
            _timeForExecute = 0;
            _rows = 0;
        }

        private void CountRows(int row, int column)
        {
            _researchTeam = new ResearchTeam[row][];
            var elements = row * column;
            while (_rows < row)
            {
                _researchTeam[_rows] = new ResearchTeam[_rows + 1];
                elements -= _rows + 1;
                if (elements < _rows || _rows + 1 == row)
                {
                    _researchTeam[_rows] = new ResearchTeam[elements];
                    break;
                }
                _rows++;
            }
        }

        public void CreateArray(int row, int column)
        {
            CountRows(row, column);
            var startTrackTime = Environment.TickCount;
            for (var i = 0; i < _researchTeam.Length; i++)
            {
                for (var j = 0; j < _researchTeam[i].Length; j++)
                {
                    _researchTeam[i][j] = new ResearchTeam();
                }
            }
            var endTrackTime = Environment.TickCount;
            _timeForExecute = endTrackTime - startTrackTime;

        }

        public int ShowTimeResult()
        {
            return _timeForExecute;
        }
    }
}
