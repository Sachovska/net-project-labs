﻿using System;
using DataAccessLayer.Interfaces;
using Entities.Models;

namespace DataAccessLayer.Providers
{
    public class OneSizeArrayProvider : IArrayProvider
    {
        private ResearchTeam[] _researchTeam;
        private int _timeForExecute;

        public OneSizeArrayProvider()
        {
            _researchTeam = new ResearchTeam[]{};
            _timeForExecute = 0;
        }

        public void CreateArray(int row, int column)
        {
            _researchTeam = new ResearchTeam[row*column];
            var startTrackTime = Environment.TickCount;
            for (var i = 0; i < _researchTeam.Length; i++)
            {
                _researchTeam[i] = new ResearchTeam();
            }
            var endTrackTime = Environment.TickCount;
            _timeForExecute = endTrackTime - startTrackTime;
        }

        public int ShowTimeResult()
        {
            return _timeForExecute;
        }
    }
}
