﻿using System;
using System.ComponentModel.DataAnnotations;
using DataAccessLayer.SerealizationProviders;
using Entities.CustomException;
using Entities.Events;
using Entities.Journals;
using Entities.ModelsList;
using Entities.ModelsMemoryStream;
using Entities.RefModels;

namespace TestProject
{
    public class Program
    {
        #region Lab_4(additional)

        //public static TeamsJournal FirstTeamJournal { get; set; }
        //public static TeamsJournal SecondTeamJournal { get; set; }
        //public static void Show_Message(object source, TeamListHandlerEventArgs args)
        //{
        //    TeamsJournalEntry teamsJournalEntry = new TeamsJournalEntry(args.NameOfCollection, args.TypeOfVariables, args.IndexOfElement);

        //    if (teamsJournalEntry.NameOfCollection == "firstCollection")
        //    {
        //        FirstTeamJournal.TeamsJournals.Add(teamsJournalEntry);
        //    }
        //    else
        //    {
        //        SecondTeamJournal.TeamsJournals.Add(teamsJournalEntry);
        //    }
        //}

        #endregion

        static void Main()
        {
            #region Lab_1

            //var researchTeam = new ResearchTeam();
            //Console.WriteLine(researchTeam.ToShortString());
            //Console.WriteLine(researchTeam[TimeFrame.Long]);
            //Console.WriteLine(researchTeam[TimeFrame.TwoYears]);

            //Console.WriteLine("New research team: ");
            //researchTeam.Id = 2;
            //researchTeam.Theme = "New Theme";
            //researchTeam.Organization = "New Org";
            //researchTeam.Number = 112;
            //researchTeam.TimeResearch = TimeFrame.Long;
            //researchTeam.Papers = new[] { new Paper() };
            //Console.WriteLine(researchTeam.ToString());

            //Console.WriteLine("Added paper using method: ");
            //var paper = new Paper(3, "NewName", new Person(), DateTime.Today);
            //researchTeam.AddPaper(paper);
            //Console.WriteLine(researchTeam.ToString());
            //Console.WriteLine($"Last publish: {researchTeam.LastPublish}");

            //
            //Console.WriteLine("Enter amount of rows, than space and amount of columns");
            //var sizeArray = Console.ReadLine();
            //var row = Convert.ToInt32(sizeArray.Split(' ')[0]);
            //var column = Convert.ToInt32(sizeArray.Split(' ')[1]);
            //var arrayOneSize = new OneSizeArrayProvider();
            //arrayOneSize.CreateArray(row, column);
            //Console.WriteLine($"Time for one size array: {arrayOneSize.ShowTimeResult()}");

            //var arrayTwoSize = new TwoSizeArrayProvider();
            //arrayTwoSize.CreateArray(row, column);
            //Console.WriteLine($"Time for two size array: {arrayTwoSize.ShowTimeResult()}");

            //var arrayRectangular = new RectangularSizeArray();
            //arrayRectangular.CreateArray(row, column);
            //Console.WriteLine($"Time for rectangular array: {arrayRectangular.ShowTimeResult()}");

            #endregion

            #region Lab_2

            //var teamFirst = new Team();
            //var teamSecond = new Team();
            //Console.WriteLine(teamFirst.Equals(teamSecond));
            //Console.WriteLine($"Hash code first team {teamFirst.GetHashCode()}");
            //Console.WriteLine($"Hash code second team {teamSecond.GetHashCode()}");
            //try
            //{
            //    var invalidTeam = new Team("Orf", -234);

            //}
            //catch (Exception ex)
            //{
            //    Console.WriteLine(ex.Message);
            //}
            //var researchTeam = new Entities.ModelsCopy.ResearchTeam();
            //Console.WriteLine(researchTeam.ToString());
            //var team = researchTeam.GetTeam;
            //Console.WriteLine(
            //    $"Team {team}");

            //var copyResearchTeam = researchTeam.DeepCopy();
            //researchTeam.Id = 2;
            //Console.WriteLine($"Original research team: {researchTeam}");
            //Console.WriteLine($"Copy research team: {copyResearchTeam}");
            //Console.WriteLine("Persons without papers: ");
            //foreach (Person person in researchTeam)
            //{
            //    Console.WriteLine(person);
            //}

            //Console.WriteLine("Latest papers in two years: ");
            //foreach (Paper paper in researchTeam.GetPublishingDuringTerm(2))
            //{
            //    Console.WriteLine(paper);
            //}

            #endregion

            #region Lab_3

            //var researchTeams = new ResearchTeamCollection();
            //researchTeams.AddDefaults();
            //Console.WriteLine(researchTeams.ToString());

            //Console.WriteLine("\nSort by number of registration");
            //researchTeams.SortByNumber();
            //Console.WriteLine(researchTeams.ToString());
            //Console.WriteLine("\nSort by theme of research");
            //researchTeams.SortByTheme();
            //Console.WriteLine(researchTeams.ToString());
            //Console.WriteLine("\nSort by count of papers");
            //researchTeams.SortByCountOfPublishing();
            //Console.WriteLine(researchTeams.ToString());


            //Console.WriteLine("\nMin number");
            //Console.WriteLine(researchTeams.GetMinInRegistrationNumber);
            //Console.WriteLine("\nSort by time of researching");
            //var longReasearchTeams = researchTeams.GetTwoYearsLongResearchTeams;
            //foreach (var researchTeam in longReasearchTeams)
            //{
            //    Console.WriteLine(researchTeam.ToString());
            //}
            //Console.WriteLine("\nGroup by count of publish");
            //var groupReasearchTeams = researchTeams.NGroup(1);
            //foreach (var researchTeam in groupReasearchTeams)
            //{
            //    Console.WriteLine(researchTeam.ToString());
            //}

            //Console.WriteLine();
            //var testCollections = new TestCollection();
            //testCollections.GetResearchTeam(1);

            #endregion

            #region Lab_4

            //var firstCollection = new ResearchTeamCollection();
            //var secondCollection = new ResearchTeamCollection();
            //firstCollection.AddDefaults();
            //secondCollection.AddDefaults();
            //FirstTeamJournal = new TeamsJournal();
            //SecondTeamJournal = new TeamsJournal();

            //firstCollection.NameOfCollection = nameof(firstCollection);
            //secondCollection.NameOfCollection = nameof(secondCollection);
            //FirstTeamJournal.NameOfJournal = nameof(FirstTeamJournal);
            //SecondTeamJournal.NameOfJournal = nameof(SecondTeamJournal);

            //firstCollection.ResearchTeamAdded += Show_Message;
            //firstCollection.ResearchTeamInserted += Show_Message;

            //secondCollection.ResearchTeamAdded += Show_Message;
            //secondCollection.ResearchTeamInserted += Show_Message;

            //firstCollection.InsertAt(0, new Entities.ModelsList.ResearchTeam());
            //firstCollection.InsertAt(3, new Entities.ModelsList.ResearchTeam());

            //secondCollection.InsertAt(2, new Entities.ModelsList.ResearchTeam());
            //secondCollection.InsertAt(3, new Entities.ModelsList.ResearchTeam());
            //secondCollection.InsertAt(2, new Entities.ModelsList.ResearchTeam());

            //Console.WriteLine(FirstTeamJournal.ToString());
            //Console.WriteLine();
            //Console.WriteLine(SecondTeamJournal.ToString());

            #endregion

            #region Lab_5

            //var paperProvider = new PaperProvider();
            //var invalidPaper = new Paper(1, String.Empty, null, DateTime.Today);
            //try
            //{

            //    paperProvider.CreateObj("paper.txt", invalidPaper);
            //}
            //catch (ValidationException ex)
            //{
            //    Console.WriteLine(ex);
            //}
            //catch (Exception e)
            //{
            //    Console.WriteLine(e);
            //}

            //// set valid values
            //invalidPaper.Name = "ValidName";
            //invalidPaper.Author = new Person();
            //invalidPaper.DateOfPublish = Convert.ToDateTime("10-12-1998");
            //paperProvider.CreateObj("paper.json", invalidPaper);
            //var copyOfInvalidPaper = invalidPaper.DeepCopy();

            //// get valid paper from file
            //Console.WriteLine(paperProvider.GetObj("paper.json").ToString());

            //// get copy of invalid (but now valid object)
            //Console.WriteLine(copyOfInvalidPaper.ToString());

            //// add paper from console
            //paperProvider.AddFromConsole();

            //// get added paper
            //Console.WriteLine(paperProvider.GetObj("paper.txt"));

            #endregion

            #region Lab_6

            var dateTime = DateTime.Now;
            if (dateTime.DayOfWeek != DayOfWeek.Sunday)
            {
                var human = new Human();
                Human[] humans = { new Student(), new Botan(), new Girl(), new PrettyGirl(), new SmartGirl() };
                ConsoleKey readKey;
                do
                {
                    var random = new Random();
                    var firstIndex = random.Next(humans.Length);
                    var secondIndex = random.Next(humans.Length);

                    Console.WriteLine("First instance: " + humans[firstIndex].GetType().Name);
                    Console.WriteLine("Second instance: " + humans[secondIndex].GetType().Name );
                    try
                    {
                        Human.ValidateCouple(humans[firstIndex], humans[secondIndex]);
                    }
                    catch (InvalidCoupleArguments e)
                    {
                        Console.WriteLine(e.Message);
                    }

                    var child = human.Couple(humans[firstIndex], humans[secondIndex]);

                    Console.WriteLine("Name: " + child.GetType().GetProperty("Name")?.GetValue(child));
                    Console.WriteLine("Surname: " + child.GetType().GetProperty("Surname")?.GetValue(child));
                    Console.WriteLine("ChildType: " + child);
                    readKey = Console.ReadKey(false).Key;
                } while (readKey != ConsoleKey.F10 && readKey != ConsoleKey.Q && readKey.ToString() != "q");
            }
            else
            {
                Console.WriteLine("Opps!");
            }

            #endregion

            Console.ReadKey();
        }
    }
}
