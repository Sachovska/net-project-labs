﻿using System;

namespace Entities.Enums
{
    public class Enum
    {
        [Serializable]
        public enum TimeFrame
        {
            Year,
            TwoYears,
            Long
        }
    }
}
