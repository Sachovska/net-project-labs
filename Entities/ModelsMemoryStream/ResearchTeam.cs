﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Enum = Entities.Enums.Enum;
using System.ComponentModel.DataAnnotations;

namespace Entities.ModelsMemoryStream
{
    [DataContract]
    public class ResearchTeam : Team
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        [Required(ErrorMessage = "Theme  is required")]
        public string Theme { get; set; }

        [DataMember]
        [Required(ErrorMessage = "Time Research  is required")]
        public Enum.TimeFrame TimeResearch { get; set; }

        [DataMember]
        [Required(ErrorMessage = "Persons  is required")]
        public List<Person> Persons { get; set; }

        [DataMember]
        [Required(ErrorMessage = "Papers  is required")]
        public List<Paper> Papers { get; set; }
        
        public Team GetTeam
        {
            get => this;
            set
            {
                Organization = value.Organization;
                Number = value.Number;
            }
        }

        public ResearchTeam(int id, string theme, Enums.Enum.TimeFrame timeResearch,
            List<Person> persons, List<Paper> papers, string organization, int number) : base(organization, number)
        {
            Id = id;
            Theme = theme;
            TimeResearch = timeResearch;
            Persons = persons;
            Papers = papers;
        }

        public ResearchTeam()
        {
            Id = 1;
            Theme = "Theme";
            TimeResearch = Enums.Enum.TimeFrame.Year;
            Persons = new List<Person> { new Person() };
            Papers = new List<Paper> { new Paper() };
            GetTeam = new Team();
        }

        public Paper LastPublish => Papers.Count == 0 ? null : Papers.Last();

        public bool this[Enum.TimeFrame value] => TimeResearch.Equals(value);

        private StringBuilder GetPapers()
        {
            var papers = new StringBuilder();
            foreach (var paper in Papers)
            {
                papers.Append($"{{{paper}}}");
            }
            return papers;
        }

        private StringBuilder GetPersons()
        {
            var persons = new StringBuilder();
            foreach (var person in Persons)
            {
                persons.Append($"{{{person}}}");
            }
            return persons;
        }


        public override string ToString()
        {
            return
                $"Id: {Id}; Theme: {Theme}; TimeResearch: {TimeResearch}; Team: {{Number: {GetTeam.Number},  {GetTeam.Organization}}}; Persons: {GetPersons()}; Papers: {GetPapers()}";
        }

        public string ToShortString()
        {
            return
                $"Id: {Id}; Theme: {Theme}; TimeResearch: {TimeResearch}";
        }

        public override bool Equals(object obj)
        {
            return obj is ResearchTeam team &&
                   Id == team.Id &&
                   Theme == team.Theme &&
                   TimeResearch == team.TimeResearch &&
                   EqualityComparer<List<Person>>.Default.Equals(Persons, team.Persons) &&
                   EqualityComparer<List<Paper>>.Default.Equals(Papers, team.Papers) &&
                   EqualityComparer<Paper>.Default.Equals(LastPublish, team.LastPublish);
        }

        public override int GetHashCode()
        {
            var hashCode = 1554624952;
            hashCode = hashCode * -1521134295 + Id.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Theme);
            hashCode = hashCode * -1521134295 + TimeResearch.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<List<Person>>.Default.GetHashCode(Persons);
            hashCode = hashCode * -1521134295 + EqualityComparer<List<Paper>>.Default.GetHashCode(Papers);
            hashCode = hashCode * -1521134295 + EqualityComparer<Paper>.Default.GetHashCode(LastPublish);
            return hashCode;
        }

        public IEnumerable<Paper> GetPublishingDuringTerm(int n)
        {
            if (n < 0)
            {
                yield break;
            }

            var now = DateTime.Now;
            var deltaTime = now.AddYears(-n);
            foreach (var paper in Papers)
            {
                if (deltaTime < paper.DateOfPublish)
                {
                    yield return paper;
                }
            }
        }

        public static bool operator !=(ResearchTeam researchTeam1, ResearchTeam researchTeam2)
        {
            return !(researchTeam1 == researchTeam2);
        }

        public static bool operator ==(ResearchTeam researchTeam1, ResearchTeam researchTeam2)
        {
            if (ReferenceEquals(researchTeam1, researchTeam2))
            {
                return true;
            }
            if (ReferenceEquals(researchTeam1, null))
            {
                return false;
            }
            if (ReferenceEquals(researchTeam2, null))
            {
                return false;
            }
            return researchTeam1.Theme == researchTeam2.Theme &&
                   researchTeam1.Number == researchTeam2.Number &&
                   researchTeam1.TimeResearch == researchTeam2.TimeResearch &&
                   Paper.Equals(researchTeam1.Papers, researchTeam2.Papers);
        }
    }
}
