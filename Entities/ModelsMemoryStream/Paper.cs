﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Entities.Attributes;

namespace Entities.ModelsMemoryStream
{
    [DataContract]
    public class Paper
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        [Required(ErrorMessage = "Name is required")]
        [StringLength(80, ErrorMessage = "Name max size must be 80 characters")]
        public string Name { get; set; }

        [DataMember]
        [Required(ErrorMessage = "Author is required")]
        public Person Author { get; set; }

        [DataMember]
        [Required(ErrorMessage = "Date of publish is required")]
        [DateTimePast(ErrorMessage = "DateOfpublish must be past")]
        public DateTime DateOfPublish { get; set; }

        public Paper(int id, string name, Person author, DateTime dateOfPublish)
        {
            Id = id;
            Name = name;
            Author = author;
            DateOfPublish = dateOfPublish;
        }

        public Paper()
        {
        }

        public override string ToString()
        {
            return
                $"Id: {Id}; Name: {Name}; Author: {Author.Id}, {Author.Name}, {Author.Surname}, {Author.DateOfBirthday}; DateOfPublish: {DateOfPublish}";
        }

        public virtual Paper DeepCopy()
        {
            var copy = (Paper)MemberwiseClone();
            copy.Id = Id;
            copy.DateOfPublish = DateOfPublish;
            copy.Name = string.Copy(Name);
            copy.Author = Author;
            return copy;
        }

        public override bool Equals(object obj)
        {
            return obj is Paper paper &&
                   Id == paper.Id &&
                   Name == paper.Name &&
                   EqualityComparer<Person>.Default.Equals(Author, paper.Author) &&
                   DateOfPublish == paper.DateOfPublish;
        }

        public override int GetHashCode()
        {
            var hashCode = -1582180709;
            hashCode = hashCode * -1521134295 + Id.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Name);
            hashCode = hashCode * -1521134295 + EqualityComparer<Person>.Default.GetHashCode(Author);
            hashCode = hashCode * -1521134295 + DateOfPublish.GetHashCode();
            return hashCode;
        }

        public static bool operator !=(Paper paper1, Paper paper2)
        {
            return !(paper1 == paper2);
        }

        public static bool operator ==(Paper paper1, Paper paper2)
        {
            return !(paper1 != paper2);
        }
    }
}
