﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Entities.ModelsMemoryStream
{
    [DataContract]
    public class Team
    {
        [DataMember]
        public string Organization { get; set; }
        [DataMember]
        public int Number { get; set; }

        public Team(string organization, int number)
        {
            Organization = organization;
            Number = number;
        }

        public Team()
        {
            Number = 111;
            Organization = "Organization1";
        }

        public override string ToString()
        {
            return $"Oranization: {Organization}; Number: {Number}";
        }

        public string Name { get; set; }

        public override bool Equals(object obj)
        {
            return obj is Team team &&
                   Organization == team.Organization &&
                   Number == team.Number &&
                   Name == team.Name;
        }

        public override int GetHashCode()
        {
            var hashCode = 181986416;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Organization);
            hashCode = hashCode * -1521134295 + Number.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Name);
            return hashCode;
        }
    }
}
