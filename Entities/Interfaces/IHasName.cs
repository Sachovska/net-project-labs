﻿namespace Entities.Interfaces
{
    public interface IHasName
    {
        string Name { get; }
    }
}
