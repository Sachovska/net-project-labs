﻿namespace Entities.Interfaces
{
    public interface INameAndCopy<T>
    {
        string Name { get; set; }
        T DeepCopy();
    }
}
