﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Events;
using Entities.Handlers;
using Entities.ModelsCopy;
using Enum = Entities.Enums.Enum;

namespace Entities.ModelsList
{
    public class ResearchTeamCollection
    {
        public event Delegates.TeamListHandler ResearchTeamAdded;
        public event Delegates.TeamListHandler ResearchTeamInserted;
        public string NameOfCollection { get; set; }
        public List<ResearchTeam> ResearchTeams { get; private set; }

        public int GetMinInRegistrationNumber => ResearchTeams.Select(r => r.Number).Min();

        public List<ResearchTeam> GetTwoYearsLongResearchTeams => (from t in ResearchTeams
            where t.TimeResearch == Enum.TimeFrame.TwoYears
            select t).ToList();

        public List<ResearchTeam> NGroup(int value) => (from t in ResearchTeams
            where t.Persons.Count == value
            select t).ToList();

        public void AddDefaults()
        {
            var researchTeamFirst = new ResearchTeam();
            var researchTeamSecond = new ResearchTeam
            {
                Id = 2,
                Number = 2,
                Theme = "aaa",
                TimeResearch = Enum.TimeFrame.TwoYears,
                GetTeam = new Team("FF", 223)
            };
            researchTeamSecond.AddPaper(new Paper());
            ResearchTeams = new List<ResearchTeam> {researchTeamFirst, researchTeamSecond};

            ResearchTeamAdded?.Invoke(researchTeamFirst,
                new TeamListHandlerEventArgs(NameOfCollection, "Added", ResearchTeams.Count - 1));
            ResearchTeamAdded?.Invoke(researchTeamSecond,
                new TeamListHandlerEventArgs(NameOfCollection, "Added", ResearchTeams.Count - 1));
        }

        public void AddResearchTeams(params ResearchTeam[] researchTeams)
        {
            foreach (var researchTeam in researchTeams)
            {
                ResearchTeams.Add(researchTeam);
                ResearchTeamAdded?.Invoke(researchTeam,
                    new TeamListHandlerEventArgs(NameOfCollection, "Added", ResearchTeams.Count - 1));
            }
        }

        public void SortByNumber()
        {
            ResearchTeams.Sort();
        }

        public void SortByTheme()
        {
            ResearchTeams.Sort(new ResearchTeam());
        }

        public void SortByCountOfPublishing()
        {
            ResearchTeams.Sort(new ResearchTeamHelper());
        }

        public void InsertAt(int index, ResearchTeam researchTeam)
        {
            if (index >= ResearchTeams.Count)
            {
                ResearchTeams.Add(researchTeam);
                ResearchTeamAdded?.Invoke(researchTeam,
                    new TeamListHandlerEventArgs(NameOfCollection, "Added", ResearchTeams.Count - 1));
            }
            else
            {
                ResearchTeams.Insert(index, researchTeam);
                ResearchTeamInserted?.Invoke(researchTeam,
                    new TeamListHandlerEventArgs(NameOfCollection, "Inserted", ResearchTeams.Count - 1));
            }
        }

        public ResearchTeam this[int index]
        {
            get => index > 0
                ? ResearchTeams[index]
                : throw new ArgumentOutOfRangeException($"Index has to be > 0");
            set => ResearchTeams[index] = value;
        }

        public override string ToString()
        {
            var stringBuilder = new StringBuilder();
            foreach (var researchTeam in ResearchTeams)
            {
                stringBuilder.Append(researchTeam + "\n");
            }
            return stringBuilder.ToString();
        }

        public virtual string ToShortString()
        {
            var stringBuilder = new StringBuilder();
            foreach (var researchTeam in ResearchTeams)
            {
                stringBuilder.Append(researchTeam.ToShortString() + "\n");
            }
            return stringBuilder.ToString();
        }
    }
}
