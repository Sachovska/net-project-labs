﻿using System;
using System.Collections.Generic;
using Entities.Interfaces;

namespace Entities.ModelsList
{
    public class Team : INameAndCopy<Team>, IComparable<Team>
    {
        public string Organization { get; set; }
        public int Number { get; set; }

        public Team(string organization, int number)
        {
            Number = 22;
            Organization = organization;
        }

        public Team()
        {
            Number = 111;
            Organization = "Organization1";
        }

        public override string ToString()
        {
            return $"Oranization: {Organization}; Number: {Number}";
        }

        public string Name { get; set; }

        public override bool Equals(object obj)
        {
            return obj is Team team &&
                   Organization == team.Organization &&
                   Number == team.Number &&
                   Name == team.Name;
        }

        public override int GetHashCode()
        {
            var hashCode = 181986416;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Organization);
            hashCode = hashCode * -1521134295 + Number.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Name);
            return hashCode;
        }

        public Team DeepCopy()
        {
            var copy = (Team)MemberwiseClone();
            copy.Organization = String.Copy(Organization);
            copy.Number = Number;
            return copy;
        }

        public int CompareTo(Team other)
        {
            if (ReferenceEquals(this, other)) return 0;
            if (ReferenceEquals(null, other)) return 1;
            return Number.CompareTo(other.Number);
        }
    }

}
