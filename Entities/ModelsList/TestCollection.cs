﻿using System;
using System.Collections.Generic;

namespace Entities.ModelsList
{
    public class TestCollection
    {
        public List<Team> Teams { get; set; }
        public List<string> Strings { get; set; }
        public Dictionary<Team, ResearchTeam> TeamsResearchDictionary { get; set; }
        public Dictionary<string, ResearchTeam> ResearchDictionary { get; set; }

        public TestCollection()
        {
            Teams = new List<Team>();
            Strings = new List<string>();
            TeamsResearchDictionary = new Dictionary<Team, ResearchTeam>();
            ResearchDictionary = new Dictionary<string, ResearchTeam>();
        }
        public TestCollection(List<Team> teams, List<string> stringList, Dictionary<Team, ResearchTeam> tDictionary, Dictionary<string, ResearchTeam> sDictionary)
        {
            Teams = teams;
            Strings = stringList;
            TeamsResearchDictionary = tDictionary;
            ResearchDictionary = sDictionary;
        }

        public void GetResearchTeam(int index)
        {
            const int amount = 5000;
            var testCollections = new TestCollection();
            for (var i = 1; i <= amount; i++)
            {
                var researchTeam = new ResearchTeam {Id = i};
                researchTeam.Name = researchTeam.Name + i;
                var team = new Team("Fantastic fours" + i, i);
                var teamString = team.ToString();
                testCollections.Teams.Add(team);
                testCollections.Strings.Add(teamString);
                testCollections.TeamsResearchDictionary.Add(team, researchTeam);
                testCollections.ResearchDictionary.Add(teamString, researchTeam);
            }

            var suggestedTeam = new Team("Fantastic fours" + index, index);
            var suggestedResearchTeam = new ResearchTeam {Id = 2};
            suggestedResearchTeam.Name = suggestedResearchTeam.Name + 2;

            Console.WriteLine("Suggested team: ");
            Console.WriteLine(suggestedTeam);
            Console.WriteLine("Suggested researchTeam: ");
            Console.WriteLine(suggestedResearchTeam);

            var start = Environment.TickCount;
            if (testCollections.Teams.Contains(suggestedTeam))
                Console.WriteLine("Teams contain suggested team");
            var end = Environment.TickCount;
            Console.WriteLine("Suggested team in Teams: " + (end - start));

            start = Environment.TickCount;

            if (testCollections.Strings.Contains(suggestedTeam.ToString()))
                Console.WriteLine("Strings contain suggested team");

            end = Environment.TickCount;
            Console.WriteLine("Suggested team in strings: " + (end - start));

            start = Environment.TickCount;
            if (testCollections.TeamsResearchDictionary.ContainsKey(suggestedTeam))
                if (suggestedResearchTeam == testCollections.TeamsResearchDictionary[suggestedTeam])
                    Console.WriteLine("Team ResearchTeam Dictionary contains suggested team and dictionary");
            end = Environment.TickCount;
            Console.WriteLine("Dictionary researchTeam find (key): " + (end - start));

            start = Environment.TickCount;

            if (testCollections.ResearchDictionary.ContainsKey(suggestedTeam.ToString()))
                if (suggestedResearchTeam == testCollections.ResearchDictionary[suggestedTeam.ToString()])
                    Console.WriteLine("Research Team distionary contains suggested team and dictionary");
            end = Environment.TickCount;
            Console.WriteLine("Dictionary researchTeam find (key): " + (end - start));

        }
    }
}
