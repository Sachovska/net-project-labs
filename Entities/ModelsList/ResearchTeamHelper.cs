﻿using System.Collections.Generic;

namespace Entities.ModelsList
{
    public class ResearchTeamHelper:ResearchTeam, IComparer<ResearchTeamHelper>
    {
        public int Compare(ResearchTeamHelper x, ResearchTeamHelper y)
        {
            if (ReferenceEquals(x.Papers, null))
                return 1;
            if (ReferenceEquals(y.Papers, null))
                return -1;

            if (x.Papers.Count > y.Papers.Count)
                return 1;
            if (x.Papers.Count < y.Papers.Count)
                return -1;

            return 0;
        }
    }
}
