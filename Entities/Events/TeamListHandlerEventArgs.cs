﻿using System;

namespace Entities.Events
{
    public class TeamListHandlerEventArgs : EventArgs
    {

        public string NameOfCollection { get; set; }
        public string TypeOfVariables { get; set; }
        public int IndexOfElement { get; set; }

        public TeamListHandlerEventArgs(string nameOfCollection, string typeOfVariables, int indexOfElement)
        {
            NameOfCollection = nameOfCollection;
            TypeOfVariables = typeOfVariables;
            IndexOfElement = indexOfElement;
        }

        public override string ToString()
        {
            return
                $"NameOfCollection: {NameOfCollection}, TypeOfVariables: {TypeOfVariables}, IndexOfElement: {IndexOfElement}";
        }
    }
}
