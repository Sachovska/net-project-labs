﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public class Person
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public DateTime DateOfBirthday { get; set; }

        public Person(int id, string name, string surname, DateTime dateOfBirthday)
        {
            Id = id;
            Name = name;
            Surname = surname;
            DateOfBirthday = dateOfBirthday;
        }

        public Person()
        {
            Id = 1;
            Name = "Vitalina";
            Surname = "Sachovska";
            DateOfBirthday = Convert.ToDateTime("04-07-1998");
        }

        public int ChangeYear
        {
            set => DateOfBirthday = new DateTime(value, DateOfBirthday.Month, DateOfBirthday.Day);
            get => DateOfBirthday.Year;
        }

        public override string ToString()
        {
            return $"Id: {Id}; Name: {Name}; Surname: {Surname}; DateOfBirthday: {DateOfBirthday}";
        }

        public virtual string ToShortString()
        {
            return $"Id: {Id}; Name: {Name}; Surname: {Surname}";
        }

        public override bool Equals(object obj)
        {
            return obj is Person person &&
                   Id == person.Id &&
                   Name == person.Name &&
                   Surname == person.Surname &&
                   DateOfBirthday == person.DateOfBirthday &&
                   ChangeYear == person.ChangeYear;
        }

        public override int GetHashCode()
        {
            var hashCode = -139126827;
            hashCode = hashCode * -1521134295 + Id.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Name);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Surname);
            hashCode = hashCode * -1521134295 + DateOfBirthday.GetHashCode();
            hashCode = hashCode * -1521134295 + ChangeYear.GetHashCode();
            return hashCode;
        }
        
        public static bool operator !=(Person person1, Person person2)
        {
            return !(person1 == person2);
        }

        public static bool operator ==(Person person1, Person person2)
        {
            return !(person1 != person2);
        }
    }
}
