﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Entities.Enums.Enum;

namespace Entities.Models
{
    public class ResearchTeam
    {
        public int Id { get; set; }
        public string Theme { get; set; }
        public string Organization { get; set; }
        public int Number { get; set; }
        public TimeFrame TimeResearch { get; set; } 
        public Paper[] Papers { get; set; }

        public ResearchTeam(int id, string theme, string organization, int number, TimeFrame timeResearch,
            Paper[] papers)
        {
            Id = id;
            Theme = theme;
            Organization = organization;
            Number = number;
            TimeResearch = timeResearch;
            Papers = papers;
        }

        public ResearchTeam()
        {
            Id = 1;
            Theme = "Theme";
            Organization = "Organization";
            Number = 111;
            TimeResearch = TimeFrame.Year;
            Papers = new[]{new Paper()};
        }

        public Paper LastPublish => Papers.Length == 0 ? null : Papers.Last();

        public bool this[TimeFrame value] => TimeResearch.Equals(value);

        public void AddPaper(params Paper[] papers)
        {
            Papers = Papers.Concat(papers).ToArray();
        }

        private StringBuilder GetPapers()
        {
            var papers = new StringBuilder();
            foreach (var paper in Papers)
            {
                papers.Append($"{{{paper}}}");
            }
            return papers;
        }

        public override string ToString()
        {
            return
                $"Id: {Id}; Theme: {Theme}; Organization: {Organization}; Number: {Number}; TimeResearch: {TimeResearch}; Papers: {GetPapers()}";
        }

        public string ToShortString()
        {
            return
                $"Id: {Id}; Theme: {Theme}; Organization: {Organization}; Number: {Number}; TimeResearch: {TimeResearch}";
        }

        public override bool Equals(object obj)
        {
            return obj is ResearchTeam team &&
                   Id == team.Id &&
                   Theme == team.Theme &&
                   Organization == team.Organization &&
                   Number == team.Number &&
                   TimeResearch == team.TimeResearch &&
                   EqualityComparer<Paper[]>.Default.Equals(Papers, team.Papers) &&
                   EqualityComparer<Paper>.Default.Equals(LastPublish, team.LastPublish);
        }

        public override int GetHashCode()
        {
            var hashCode = 1554624952;
            hashCode = hashCode * -1521134295 + Id.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Theme);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Organization);
            hashCode = hashCode * -1521134295 + Number.GetHashCode();
            hashCode = hashCode * -1521134295 + TimeResearch.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<Paper[]>.Default.GetHashCode(Papers);
            hashCode = hashCode * -1521134295 + EqualityComparer<Paper>.Default.GetHashCode(LastPublish);
            return hashCode;
        }

        public static bool operator !=(ResearchTeam researchTeam1, ResearchTeam researchTeam2)
        {
            return !(researchTeam1 == researchTeam2);
        }

        public static bool operator ==(ResearchTeam researchTeam1, ResearchTeam researchTeam2)
        {
            return !(researchTeam1 != researchTeam2);
        }
    }
}
