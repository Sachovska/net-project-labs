﻿using System;

namespace Entities.Attributes
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = false)]
    public class Couple : Attribute
    {
        public string Pair { get; set; }
        public int Probability { get; set; }
        public string ChildType { get; set; }
    }
}
