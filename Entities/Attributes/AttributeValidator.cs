﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Entities.Attributes
{
    public class AttributeValidator
    {
        private ValidationContext _validationContext;
        private List<ValidationResult> _validationResults;

        public bool IsValid<T>(T obj)
        {
            _validationContext = new ValidationContext(obj);
            _validationResults = new List<ValidationResult>();
            return Validator.TryValidateObject(obj, _validationContext, _validationResults, true);
        }

        public void Validate<T>(T obj)
        {
            if (IsValid(obj) == false)
            {
                throw new ValidationException(_validationResults[0].ErrorMessage);
            }
        }
    }
}
