﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Entities.Attributes
{
    internal class DateTimePastAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            if (value == null)
            {
                return true;
            }
            var date = (DateTime)value;
            return date <= DateTime.Now;
        }
    }
}