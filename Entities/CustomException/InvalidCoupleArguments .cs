﻿using System;

namespace Entities.CustomException
{
    public class InvalidCoupleArguments : Exception
    {
        public InvalidCoupleArguments(string message) : base(message)
        {
        }
    }
}
