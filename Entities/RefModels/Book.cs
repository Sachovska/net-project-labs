﻿using Entities.Interfaces;

namespace Entities.RefModels
{
    public class Book : IHasName
    {
        public string Name { get; }

        public Book()
        {
            Name = "Book";
        }

        public Book(string name)
        {
            Name = name;
        }
    }
}
