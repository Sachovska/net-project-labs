﻿using Entities.Events;

namespace Entities.Handlers
{
    public class Delegates
    {
        public delegate void TeamListHandler(object source, TeamListHandlerEventArgs args);
    }
}
