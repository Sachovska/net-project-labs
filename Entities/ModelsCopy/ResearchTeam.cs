﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Interfaces;
using static Entities.Enums.Enum;

namespace Entities.ModelsCopy
{
    public class ResearchTeam : Team, INameAndCopy<ResearchTeam>, IEnumerable
    {
        public int Id { get; set; }
        public string Theme { get; set; }
        public TimeFrame TimeResearch { get; set; }
        public List<Person> Persons { get; set; }
        public List<Paper> Papers { get; set; }
        public new string Name { get => base.Name; set => base.Name = value; }
        public Team GetTeam
        {
            get => this;
            set
            {
                base.Name = value.Organization;
                Number = value.Number;
            }
        }

        public ResearchTeam(int id, string theme, TimeFrame timeResearch,
            List<Person> persons, List<Paper> papers, string organization, int number):base(organization, number)
        {
            Id = id;
            Theme = theme;
            TimeResearch = timeResearch;
            Persons = persons;
            Papers = papers;
            GetTeam = new Team();
        }

        public ResearchTeam()
        {
            Id = 1;
            Theme = "Theme";
            TimeResearch = TimeFrame.Year;
            Persons = new List<Person> {new Person()};
            Papers = new List<Paper> {new Paper()};
        }

        public Paper LastPublish => Papers.Count == 0 ? null : Papers.Last();

        public bool this[TimeFrame value] => TimeResearch.Equals(value);

        public void AddPaper(params Paper[] papers)
        {
            foreach (var paper in papers)
            {
                Papers.Add(paper);
            }
        }

        private StringBuilder GetPapers()
        {
            var papers = new StringBuilder();
            foreach (var paper in Papers)
            {
                papers.Append($"{{{paper}}}");
            }
            return papers;
        }

        private StringBuilder GetPersons()
        {
            var persons = new StringBuilder();
            foreach (var person in Persons)
            {
                persons.Append($"{{{person}}}");
            }
            return persons;
        }

        public override string ToString()
        {
            return
                $"Id: {Id}; Theme: {Theme}; TimeResearch: {TimeResearch}; Persons: {GetPersons()}; Papers: {GetPapers()}";
        }

        public string ToShortString()
        {
            return
                $"Id: {Id}; Theme: {Theme}; TimeResearch: {TimeResearch}";
        }

        public override bool Equals(object obj)
        {
            return obj is ResearchTeam team &&
                   Id == team.Id &&
                   Theme == team.Theme &&
                   TimeResearch == team.TimeResearch &&
                   EqualityComparer<List<Person>>.Default.Equals(Persons, team.Persons) &&
                   EqualityComparer<List<Paper>>.Default.Equals(Papers, team.Papers) &&
                   EqualityComparer<Paper>.Default.Equals(LastPublish, team.LastPublish);
        }

        public override int GetHashCode()
        {
            var hashCode = 1554624952;
            hashCode = hashCode * -1521134295 + Id.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Theme);
            hashCode = hashCode * -1521134295 + TimeResearch.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<List<Person>>.Default.GetHashCode(Persons);
            hashCode = hashCode * -1521134295 + EqualityComparer<List<Paper>>.Default.GetHashCode(Papers);
            hashCode = hashCode * -1521134295 + EqualityComparer<Paper>.Default.GetHashCode(LastPublish);
            return hashCode;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public IEnumerator<Person> GetEnumerator()
        {
            foreach (var person in Persons)
            {
                var flag = false;
                foreach (var paper in Papers)
                {
                    if (paper.Author == person)
                    {
                        flag = true;
                        break;
                    }
                }
                if (!flag)
                    yield return person;
            }
        }

        public IEnumerable<Paper> GetPublishingDuringTerm(int n)
        {
            if (n < 0)
            {
                yield break;
            }

            var now = DateTime.Now;
            var deltaTime = now.AddYears(-n);
            foreach (var paper in Papers)
            {
                if (deltaTime < paper.DateOfPublish)
                {
                    yield return paper;
                }
            }
        }
        
        public static bool operator !=(ResearchTeam researchTeam1, ResearchTeam researchTeam2)
        {
            return !(researchTeam1 == researchTeam2);
        }

        public static bool operator ==(ResearchTeam researchTeam1, ResearchTeam researchTeam2)
        {
            return !(researchTeam1 != researchTeam2);
        }
        
        public new ResearchTeam DeepCopy()
        {
            var copy = (ResearchTeam)MemberwiseClone();
            copy.Theme = String.Copy(Theme);
            copy.TimeResearch = TimeResearch;
            copy.Papers = GetClonePapers();
            copy.Persons = GetClonePersons();
            return copy;
        }

        private List<Paper> GetClonePapers()
        {
            return Papers.Select(p => p.DeepCopy()).ToList();
        }

        private List<Person> GetClonePersons()
        {
            return Persons.Select(p => p.DeepCopy()).ToList();
        }
    }
}
