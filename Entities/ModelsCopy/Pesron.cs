﻿using System;
using System.Collections.Generic;
using Entities.Interfaces;

namespace Entities.ModelsCopy
{
    public class Person:INameAndCopy<Person>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public DateTime DateOfBirthday { get; set; }

        public Person(int id, string name, string surname, DateTime dateOfBirthday)
        {
            Id = id;
            Name = name;
            Surname = surname;
            DateOfBirthday = dateOfBirthday;
        }

        public Person()
        {
            Id = 1;
            Name = "Vitalina";
            Surname = "Sachovska";
            DateOfBirthday = Convert.ToDateTime("04-07-1998");
        }

        public int ChangeYear
        {
            set => DateOfBirthday = new DateTime(value, DateOfBirthday.Month, DateOfBirthday.Day);
            get => DateOfBirthday.Year;
        }

        public override string ToString()
        {
            return $"Id: {Id}; Name: {Name}; Surname: {Surname}; DateOfBirthday: {DateOfBirthday}";
        }

        public virtual string ToShortString()
        {
            return $"Id: {Id}; Name: {Name}; Surname: {Surname}";
        }

        public Person DeepCopy()
        {
            var copy = (Person)MemberwiseClone();
            copy.Id = Id;
            copy.Name = Name;
            copy.Surname = Surname;
            copy.DateOfBirthday = DateOfBirthday;
            return copy;
        }

        public override bool Equals(object obj)
        {
            return obj is Person person &&
                   Id == person.Id &&
                   Name == person.Name &&
                   Surname == person.Surname &&
                   DateOfBirthday == person.DateOfBirthday &&
                   ChangeYear == person.ChangeYear;
        }

        public override int GetHashCode()
        {
            var hashCode = -139126827;
            hashCode = hashCode * -1521134295 + Id.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Name);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Surname);
            hashCode = hashCode * -1521134295 + DateOfBirthday.GetHashCode();
            hashCode = hashCode * -1521134295 + ChangeYear.GetHashCode();
            return hashCode;
        }

        public static bool operator !=(Person person1, Person person2)
        {
            return !(person1 == person2);
        }

        public static bool operator ==(Person person1, Person person2)
        {
            if (ReferenceEquals(person1, person2))
            {
                return true;
            }
            if (ReferenceEquals(person2, null))
            {
                return false;
            }
            if (ReferenceEquals(person1, null))
            {
                return false;
            }
            return person1.Name == person2.Name &&
                   person1.Surname == person2.Surname &&
                   DateTime.Compare(person1.DateOfBirthday, person2.DateOfBirthday) == 0;
        }
    }
}
