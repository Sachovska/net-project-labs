﻿using System;
using System.Collections.Generic;
using Entities.Interfaces;

namespace Entities.ModelsCopy
{
    public class Team : INameAndCopy<Team>
    {
        public string Organization { get; set; }
        private int _number;
        public int Number
        {
            get => _number;
            set
            {
                if (value <= 0)
                {
                    throw new Exception("Number must be positive");
                }
                _number = Number;
            }
        }

        public Team(string organization, int number)
        {
            Organization = organization;
            _number = number;
        }

        public Team()
        {
            Number = 111;
            Organization = "Organization1";
        }
        
        public override string ToString()
        {
            return $"Oranization: {Organization}; Number: {Number}";
        }

        public string Name { get; set; }

        public override bool Equals(object obj)
        {
            return obj is Team team &&
                   Organization == team.Organization &&
                   Number == team.Number &&
                   Name == team.Name;
        }

        public override int GetHashCode()
        {
            var hashCode = 181986416;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Organization);
            hashCode = hashCode * -1521134295 + Number.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Name);
            return hashCode;
        }

        public Team DeepCopy()
        {
            var copy = (Team)MemberwiseClone();
            copy.Organization = String.Copy(Organization);
            copy.Number = Number;
            return copy;
        }
    }
}
