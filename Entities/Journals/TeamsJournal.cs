﻿using System.Collections.Generic;
using System.Text;
using Entities.Handlers;

namespace Entities.Journals
{
    public class TeamsJournal
    {
        public string NameOfJournal { get; set; }
        public List<TeamsJournalEntry> TeamsJournals { get; set; }
        public event Delegates.TeamListHandler ResearchTeamAdded;
        public event Delegates.TeamListHandler ResearchTeamInserted;


        public TeamsJournal() : this(new List<TeamsJournalEntry>()) { }
        public TeamsJournal(List<TeamsJournalEntry> journal)
        {
            TeamsJournals = journal;
        }

        public override string ToString()
        {
            var stringBuilder = new StringBuilder();
            stringBuilder.Append($"Journal: {NameOfJournal} \n");
            foreach (var entry in TeamsJournals)
            {
                stringBuilder.Append(entry + "\n");
            }

            return stringBuilder.ToString();
        }
    }
}
