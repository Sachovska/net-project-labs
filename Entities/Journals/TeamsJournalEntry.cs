﻿namespace Entities.Journals
{
    public class TeamsJournalEntry
    {
        public string NameOfCollection { get; set; }
        public string NameOfEvent { get; set; }
        public int NumberNewElement { get; set; }

        public TeamsJournalEntry(string nameOfCollection, string nameOfEvent, int numberNewElement)
        {
            NameOfCollection = nameOfCollection;
            NameOfEvent = nameOfEvent;
            NumberNewElement = numberNewElement;
        }

        public override string ToString()
        {
            return
                $"NameOfCollection: {NameOfCollection}, NameOfEvent: {NameOfEvent}, NumberNewElement: {NumberNewElement}";
        }
    }
}
